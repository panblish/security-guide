# セキュリティガイド　Microsoft Teams編

## Microsoft Teamsとは

Microsoft Teams (マイクロソフト・チームズ) は、マイクロソフトがWindows、macOS、Linux、iOS及びAndroid向けに開発・提供するコラボレーションプラットフォームである。Microsoft 365アプリケーションの一部。


### 特徴（Wikipediaより抜粋）

###### - チーム
- チームは、管理者または所有者から送信された特定のURLまたは招待を介して参加できる。Teams for Educationを使用すると、管理者と教師は、クラス、専門学習コミュニティ（PLC）、スタッフメンバー、および全員のための特定のチームを設定できる 

###### - チャネル
- チーム内で、メンバーはチャネルを設定することができる。チャネルとは、チームメンバーが電子メールやグループSMS（テキストメッセージ）を使用せずに通信できるようにする会話のトピックである。ユーザーは、テキスト、画像、 GIF 、カスタムメイドのミームを返信できる。
- ダイレクトメッセージを使用すると、ユーザーは個人のグループではなく特定のユーザーにプライベートメッセージを送信できる。
- コネクタは、チャンネルに情報を送信できるサードパーティのサービスである。コネクタには、　MailChimp、　Facebook Pages、　Twitter、　Bing Newsが含まれる。

###### - 通話
- インスタントメッセージング
- Voice over IP （VoIP）
- クライアントソフトウェア内のビデオ会議
- チームは公衆交換電話網 （PSTN）会議をサポートしており、ユーザーはクライアントから電話番号に電話をかけることができる。

###### - 会議
- 会議のスケジュールやアドホックな作成が可能で、チャンネルを訪れたユーザーは現在会議が進行中であることを確認することができる。Microsoft TeamsにはMicrosoft Outlook用のプラグインもあり、他のユーザーをMicrosoft Teamsの会議に招待することができる。

### セキュリティガイド等

 - [セキュリティと Microsoft Teams](https://docs.microsoft.com/ja-jp/microsoftteams/teams-security-guide)
 - [共有コンピューターで Microsoft Teams を安全に使用する](https://docs.microsoft.com/ja-jp/microsoftteams/shared-device-security-for-microsoft-teams)
 - [Office 365 URL および IP アドレス範囲](https://docs.microsoft.com/ja-jp/microsoft-365/enterprise/urls-and-ip-address-ranges?view=o365-worldwide)


### セキュリティ対策例
| リスク/脅威 | 対策 | 説明、実例等 | 
| --- | --- | --- | 
| 添付ファイル受信によるウィルス、マルウェア感染 | アンチウィルスソフトの導入| [アンチウィルスソフトの性能比較（英語）](https://www.av-comparatives.org/comparison/) |
| リンク共有先からのゼロディ攻撃 | Windows Updateの定期実施 | Teamsのリンクや添付ファイルから脆弱性パッチ未適応のPCを狙うゼロディ攻撃にはソフトウェア、ドライバ、OSのアップデートが有効です。[Microsoftのセキュリティ情報](https://msrc-blog.microsoft.com/category/jpsecurity/)は以下に公開されています。
| アカウント流出によるなりすましログイン| 2段階認証の有効化 | Teamsを利用するアカウントに対して2段階認証を有効にしておくことで、端末紛失時の不正アクセスを防ぎます。Googleによる[調査](https://jp.techcrunch.com/2019/05/21/2019-05-20-google-data-two-factor-security/)とSMS認証は標的型攻撃に70%以上の効果があると報告されています。設定方法は[こちら](https://docs.microsoft.com/ja-jp/microsoft-365/admin/security-and-compliance/set-up-multi-factor-authentication?view=o365-worldwide) |
| 不正ファイルの共有による情報漏えい | 共有リンクの活用 | One DriveまたはSharepointを経由でファイル交換をすることで、不正ファイルが共有された際に即座に共有停止や、[監査ログ](https://docs.microsoft.com/ja-jp/microsoft-365/compliance/search-the-audit-log-in-security-and-compliance?view=o365-worldwide)から被害の状況を確認できます。[取得方法] |
| 匿名ユーザの会議参加による情報漏えい | ロビー機能の有効化 | 会議招待をされた人以外は、ロビー経由で参加するように設定を変更します。この機能は電話から参加する人にも有効になります。|
| 画面共有時の機密情報漏洩 | 仮想デスクトップ、通知の抑制 | 参加者が自分のPCを画面共有をして発表する際に、背後にある画面やデスクトップにあるアイコンのファイル名から機密情報が公開されるおそれがあります。対策としては[仮想デスクトップの設定](https://jp.ext.hp.com/techdevice/windows10sc/54/)や通知機能の一時的にオフにする等があります。 |

等

## Pandion社のTeams情報漏えいの取り組み例
 　Microsoft Teamsは、　WindowsやOffice365と親和性が高く、 ビジネスシーンでも多く利用されている、リモート会議のツールです。
既存のインフラとの連携ができる等、多くの機能があり、有効に活用することでリモート業務を効率化することができます。
その反面、管理者や利用者への知識やリテラシーがセキュリティ事故に直結します。実際にあと一歩で事故になるという場面に遭遇したこともあります。
　しかし、 知識の拡充やリテラシー教育を一度に行うことは非現実的です。
では、どうすればよいのでしょうか。

その回答に正解はないのですが 
ここでは例としてPandion社が実際に行っているTeams運用での取り組みを紹介します。

- プロジェクト別でのTeams環境の分離（誤アップロード、誤ログインの防止）
- アクセスログとウィルスチェックのあるファイル共有サービスの利用（機密ファイルの安全確保と、漏洩時の被害把握）
- アカウントの２段階認証とスマートデバイスでの生体認証（不正ログインの防止）
- 仮想デスクトップの活用（うっかり漏洩の防止）
- リスク管理研修の参加実施（継続的セキュリティ教育）

上記は一例で案件の特性に併せてケース別に対策を行っています。


## 問い合わせ先
リスク管理、情報セキュリティの素朴な疑問、技術的な質問があれば、気軽にお問い合わせください。

[Pandion 株式会社](https://pandion.ltd)

Copyright &copy;  2022 Pandion株式会社
　
